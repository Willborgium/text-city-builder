﻿using System;

namespace TextCityBuilder.Core
{
    public class MenuOption
    {
        public string Label { get; }

        public MenuOption(string label, Action selectedAction)
        {
            Label = label;
            _selectedAction = selectedAction;
        }

        public void Select()
        {
            _selectedAction();
        }

        private readonly Action _selectedAction;
    }
}
