﻿using System;

namespace TextCityBuilder.Core
{
    public class ValidationEventArgs<TInput> : EventArgs
    {
        public TInput Input { get; }

        public bool IsValid { get; set; }

        public ValidationEventArgs(TInput input)
        {
            IsValid = true;
            Input = input;
        }
    }
}
