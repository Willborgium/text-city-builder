﻿using System;

namespace TextCityBuilder.Core
{
    public class Prompt
    {
        public event EventHandler<ValidationEventArgs<string>> Validate;

        public event Func<string, object> Convert;

        public event Action<object> ProcessResult;

        public bool IsComplete { get; set; }

        public Prompt(string label)
        {
            _label = label;
        }

        public void Render()
        {
            Console.WriteLine(_label);
        }

        public void Update()
        {
            var input = Console.ReadLine();

            var args = new ValidationEventArgs<string>(input);

            Validate?.Invoke(this, args);

            if (!args.IsValid)
            {
                Console.WriteLine("Please provide a valid value");
                    
                Console.ReadKey();

                return;
            }

            var result = Convert?.Invoke(input);

            ProcessResult?.Invoke(result);

            IsComplete = true;
        }

        private readonly string _label;
    }
}
