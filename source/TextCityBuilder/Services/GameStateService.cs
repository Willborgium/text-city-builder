﻿using System;
using System.Collections.Generic;
using TextCityBuilder.States;

namespace TextCityBuilder.Services
{
    public class GameStateService : IGameStateService
    {
        public GameStateService(IStateFactory initialStateFactory)
        {
            _initialStateFactory = initialStateFactory;
            _states = new Stack<IState>();
        }

        public void Run()
        {
            var initialState = _initialStateFactory.Create(this);

            _states.Push(initialState);

            _isRunning = true;

            while (_isRunning && _states.TryPeek(out var currentState))
            {
                Console.Clear();

                currentState.Render();

                currentState.Update();
            }

            while (_states.TryPeek(out var state))
            {
                _states.Pop();
            }
        }

        public void Quit()
        {
            _isRunning = false;
        }

        public void Push(IState nextState)
        {
            _states.Push(nextState);
        }

        public void Pop()
        {
            _states.Pop();
        }

        private readonly Stack<IState> _states;

        private readonly IStateFactory _initialStateFactory;
        private bool _isRunning;
    }
}
