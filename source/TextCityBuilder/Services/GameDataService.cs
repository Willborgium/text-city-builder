﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TextCityBuilder.Data;

namespace TextCityBuilder.Services
{
    public class GameDataService : IGameDataService
    {
        public IEnumerable<GameData> LoadGames()
        {
            if (!File.Exists("saves"))
            {
                return Enumerable.Empty<GameData>();
            }

            string data = null;

            using (var reader = new StreamReader("saves"))
            {
                data = reader.ReadToEnd();
            }

            return JsonConvert.DeserializeObject<List<GameData>>(data);
        }

        public void SaveGame(GameData data)
        {
            var games = LoadGames();

            var gamesToSave = new List<GameData>();

            var isNewGame = true;

            foreach (var game in games)
            {
                if (game.Id == data.Id)
                {
                    gamesToSave.Add(data);
                    isNewGame = false;
                }
                else
                {
                    gamesToSave.Add(game);
                }
            }

            if (isNewGame)
            {
                gamesToSave.Add(data);
            }

            var output = JsonConvert.SerializeObject(gamesToSave);

            using (var writer = new StreamWriter("saves"))
            {
                writer.Write(output);
            }
        }
    }
}
