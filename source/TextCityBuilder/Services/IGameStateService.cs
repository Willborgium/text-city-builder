﻿using TextCityBuilder.States;

namespace TextCityBuilder.Services
{
    public interface IGameStateService
    {
        public void Quit();
        public void Push(IState nextState);
        public void Pop();
    }
}
