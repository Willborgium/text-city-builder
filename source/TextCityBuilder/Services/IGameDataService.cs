﻿using System.Collections.Generic;
using TextCityBuilder.Data;

namespace TextCityBuilder.Services
{
    public interface IGameDataService
    {
        void SaveGame(GameData data);
        IEnumerable<GameData> LoadGames();
    }
}
