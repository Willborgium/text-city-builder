﻿using System;

namespace TextCityBuilder.Data
{
    public enum CityBlockType
    {
        Empty,
        Residential,
        PowerPlant,
        WaterPump,
        Entertainment,
        Factory
    }

    public class CityBlock
    {
        public CityBlockType Type { get; set; }
    }

    public class GameData
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int CurrentTurn { get; set; }

        public decimal Money { get; set; }

        public int Citizens { get; set; }

        public int CityWidth { get; set; }

        public int CityHeight { get; set; }

        public CityBlock[,] City { get; set; }
    }
}
