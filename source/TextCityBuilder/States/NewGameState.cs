﻿using System;
using System.Collections.Generic;
using System.Linq;
using TextCityBuilder.Core;
using TextCityBuilder.Data;
using TextCityBuilder.Services;

namespace TextCityBuilder.States
{
    public class NewGameState : BaseGameState
    {
        public NewGameState(
            IGameStateService gameStateService,
            IGameDataService gameDataService)
            : base(gameStateService)
        {
            var gameNamePrompt = new Prompt("Game name?");
            gameNamePrompt.Validate += OnValidateGameName;
            gameNamePrompt.Convert += OnConvertGameName;
            gameNamePrompt.ProcessResult += OnProcessGameName;

            var cityWidthPrompt = new Prompt("City Width?");
            cityWidthPrompt.Validate += OnValidateNumber;
            cityWidthPrompt.Convert += OnConvertNumber;
            cityWidthPrompt.ProcessResult += OnProcessCityWidth;

            var cityHeightPrompt = new Prompt("City Height?");
            cityHeightPrompt.Validate += OnValidateNumber;
            cityHeightPrompt.Convert += OnConvertNumber;
            cityHeightPrompt.ProcessResult += OnProcessCityHeight;

            _prompts = new List<Prompt>
            {
                gameNamePrompt,
                cityWidthPrompt,
                cityHeightPrompt
            };

            _data = new GameData
            {
                Id = Guid.NewGuid(),
                Money = 10000,
                Citizens = 0,
                CityWidth = 5,
                CityHeight = 5
            };
            
            _gameDataService = gameDataService;
        }

        private void OnProcessCityHeight(object obj)
        {
            _data.CityHeight = (int)obj;
        }

        private void OnProcessCityWidth(object obj)
        {
            _data.CityWidth = (int)obj;
        }

        private object OnConvertNumber(string arg)
        {
            return int.Parse(arg);
        }

        private void OnValidateNumber(object sender, ValidationEventArgs<string> e)
        {
            e.IsValid = int.TryParse(e.Input, out var value) && value > 0 && value < 25;
        }

        private void OnProcessGameName(object obj)
        {
            _data.Name = (string)obj;
        }

        private object OnConvertGameName(string arg)
        {
            return arg;
        }

        private void OnValidateGameName(object sender, ValidationEventArgs<string> e)
        {
            e.IsValid = !string.IsNullOrWhiteSpace(e.Input);
        }

        protected override void RenderInternal()
        {
            foreach (var prompt in _prompts)
            {
                if (prompt.IsComplete)
                {
                    continue;
                }

                prompt.Render();

                break;
            }
        }

        protected override void UpdateInternal()
        {
            foreach (var prompt in _prompts)
            {
                if (prompt.IsComplete)
                {
                    continue;
                }

                prompt.Update();

                break;
            }

            if (_prompts.All(p => p.IsComplete))
            {
                _data.City = new CityBlock[_data.CityWidth, _data.CityHeight];

                var values = Enum.GetValues<CityBlockType>();
                var r = new Random();

                for (var x = 0; x < _data.CityWidth; x++)
                {
                    for (var y = 0; y < _data.CityHeight; y++)
                    {
                        
                        _data.City[x, y] = new CityBlock
                        {
                            Type = values[r.Next(0, values.Length)]
                        };
                    }
                }

                _gameDataService.SaveGame(_data);

                GameStateService.Pop();

                GameStateService.Push(new GameState(GameStateService, _data));
            }
        }

        private readonly List<Prompt> _prompts;

        private readonly GameData _data;
        private readonly IGameDataService _gameDataService;
    }
}
