﻿using TextCityBuilder.Services;

namespace TextCityBuilder.States
{
    public interface IStateFactory
    {
        public IState Create(IGameStateService gameStateService);
    }
}
