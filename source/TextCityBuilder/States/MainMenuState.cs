﻿using System.Collections.Generic;
using TextCityBuilder.Core;
using TextCityBuilder.Data;
using TextCityBuilder.Services;

namespace TextCityBuilder.States
{
    public class LoadGameState : BaseGameState
    {
        public LoadGameState(
            IGameStateService gameStateService,
            IGameDataService gameDataService)
            : base(gameStateService)
        {
            var games = gameDataService.LoadGames();
            foreach (var game in games)
            {
                Options.Add(new MenuOption(game.Name, () => LoadGame(game)));
            }
            Options.Add(new MenuOption("Back", () => GameStateService.Pop()));
        }

        private void LoadGame(GameData data)
        {
            GameStateService.Pop();
            GameStateService.Push(new GameState(GameStateService, data));
        }
    }

    public class MainMenuState : BaseGameState
    {
        public MainMenuState(IGameStateService gameStateService)
            : base(gameStateService)
        {
            Options.Add(new MenuOption("New Game", OnNewGameSelected));
            Options.Add(new MenuOption("Load Game", OnLoadGameSelected));
            Options.Add(new MenuOption("Options", OnOptionsSelected));
            Options.Add(new MenuOption("Quit", OnQuitSelected));
        }

        private void OnNewGameSelected()
        {
            GameStateService.Push(new NewGameState(GameStateService, new GameDataService()));
        }

        private void OnLoadGameSelected()
        {
            GameStateService.Push(new LoadGameState(GameStateService, new GameDataService()));
        }

        private void OnOptionsSelected()
        {
        }

        private void OnQuitSelected()
        {
            GameStateService.Quit();
        }
    }
}
