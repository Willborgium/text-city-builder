﻿using TextCityBuilder.Services;

namespace TextCityBuilder.States
{
    public class InitialGameStateFactory : IStateFactory
    {
        public IState Create(IGameStateService gameStateService)
        {
            return new MainMenuState(gameStateService);
        }
    }
}
