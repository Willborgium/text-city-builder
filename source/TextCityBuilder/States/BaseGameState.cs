﻿using System;
using System.Collections.Generic;
using System.Linq;
using TextCityBuilder.Core;
using TextCityBuilder.Services;

namespace TextCityBuilder.States
{
    public abstract class BaseGameState : IState
    {
        protected ICollection<MenuOption> Options => _options;

        protected IGameStateService GameStateService => _gameStateService;

        protected BaseGameState(IGameStateService gameStateService)
        {
            _options = new List<MenuOption>();
            _gameStateService = gameStateService;
        }

        protected virtual void RenderInternal() { }

        public void Render()
        {
            RenderInternal();

            RenderMenu();
        }

        protected virtual void UpdateInternal() { }

        public void Update()
        {
            UpdateInternal();

            ProcessMenu();
        }

        private void RenderMenu()
        {
            if (!_options.Any())
            {
                return;
            }

            var index = 1;

            foreach (var option in _options)
            {
                Console.WriteLine($"{index++}: {option.Label}");
            }

            Console.WriteLine("What would you like to do?");
        }

        private void ProcessMenu()
        {
            if (!_options.Any())
            {
                return;
            }

            var input = Console.ReadLine();

            if (int.TryParse(input, out var selectedIndex) && selectedIndex <= _options.Count)
            {
                var option = _options[selectedIndex - 1];

                option.Select();
            }
            else
            {
                Console.WriteLine("Please make a valid selection!");

                Console.ReadKey();
            }
        }

        private readonly List<MenuOption> _options;

        private readonly IGameStateService _gameStateService;
    }
}
