﻿namespace TextCityBuilder.States
{
    public interface IState
    {
        public void Render();

        public void Update();
    }
}
