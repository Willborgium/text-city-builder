﻿using System;
using System.Diagnostics;
using System.Threading;
using TextCityBuilder.Core;
using TextCityBuilder.Data;
using TextCityBuilder.Services;

namespace TextCityBuilder.States
{
    public class ConsoleBuffer
    {
        public void Clear()
        {
            Console.Clear();
        }

        public void Write(char symbol)
        {
            Console.Write(symbol);
        }

        public void WriteLine()
        {
            Console.WriteLine();
        }

        public void WriteLine(string line)
        {
            Console.WriteLine(line);
        }

        public void ChangeConsoleColor(ConsoleColor color)
        {
            if (color == _currentConsoleColor)
            {
                return;
            }

            _currentConsoleColor = Console.ForegroundColor = color;
        }

        private ConsoleColor _currentConsoleColor = ConsoleColor.Gray;
    }

    public class GameState : BaseGameState
    {
        public GameState(
            IGameStateService gameStateService,
            GameData data)
            : base(gameStateService)
        {
            _data = data;

            Options.Add(new MenuOption("Quit", () => GameStateService.Pop()));

            _consoleBuffer = new ConsoleBuffer();
        }

        private static readonly char[] Letters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

        protected override void RenderInternal()
        { 
            Console.WriteLine($"[{_data.Id}] : {_data.Name}");
            Console.WriteLine($"[Turn {_data.CurrentTurn}]  [{_data.Money:C}]  [{_data.Citizens} citizens]");

            var bufferHeight = 3;
            var bufferWidth = 5;

            var t = new Stopwatch();

            t.Start();

            for (var y = -1; y < _data.CityHeight; y++)
            {
                for (var bufferY = 0; bufferY < bufferHeight; bufferY++)
                {
                    for (var x = -1; x < _data.CityWidth; x++)
                    {
                        for (var bufferX = 0; bufferX < bufferWidth; bufferX++)
                        {
                            _consoleBuffer.ChangeConsoleColor(ConsoleColor.Gray);

                            if (x == -1)
                            {
                                if (y > -1)
                                {
                                    if (bufferY == bufferHeight / 2)
                                    {
                                        _consoleBuffer.Write(Letters[y]);
                                    }
                                    else
                                    {
                                        _consoleBuffer.Write(' ');
                                    }
                                }
                                break;
                            }
                            else if (y == -1)
                            {
                                if (x > -1)
                                {
                                    if (bufferX == (bufferWidth / 2 + 1))
                                    {
                                        _consoleBuffer.Write(Letters[x]);
                                    }
                                    else
                                    {
                                        _consoleBuffer.Write(' ');
                                    }
                                }
                                continue;
                            }

                            var cityBlock = _data.City[x, y];

                            switch (cityBlock.Type)
                            {
                                case CityBlockType.Empty:
                                    _consoleBuffer.ChangeConsoleColor(ConsoleColor.Gray);
                                    break;

                                case CityBlockType.PowerPlant:
                                    _consoleBuffer.ChangeConsoleColor(ConsoleColor.Yellow);
                                    break;

                                case CityBlockType.Residential:
                                    _consoleBuffer.ChangeConsoleColor(ConsoleColor.DarkGreen);
                                    break;

                                case CityBlockType.Factory:
                                    _consoleBuffer.ChangeConsoleColor(ConsoleColor.DarkRed);
                                    break;

                                case CityBlockType.WaterPump:
                                    _consoleBuffer.ChangeConsoleColor(ConsoleColor.Blue);
                                    break;

                                case CityBlockType.Entertainment:
                                    _consoleBuffer.ChangeConsoleColor(ConsoleColor.Cyan);
                                    break;
                            }

                            if (bufferY == 0)
                            {
                                if (bufferX == 0)
                                {
                                    _consoleBuffer.Write('┏');
                                    continue;
                                }
                                else if (bufferX == bufferWidth - 1)
                                {
                                    _consoleBuffer.Write('┓');
                                    continue;
                                }
                                else
                                {
                                    _consoleBuffer.Write('━');
                                    continue;
                                }
                            }
                            else if (bufferY == bufferHeight - 1)
                            {
                                if (bufferX == 0)
                                {
                                    _consoleBuffer.Write('┗');
                                    continue;
                                }
                                else if (bufferX == bufferWidth - 1)
                                {
                                    _consoleBuffer.Write('┛');
                                    continue;
                                }
                                else
                                {
                                    _consoleBuffer.Write('━');
                                    continue;
                                }
                            }
                            else if (bufferX == 0 || bufferX == bufferWidth - 1)
                            {
                                _consoleBuffer.Write('┃');
                                continue;
                            }
                            else
                            {
                                _consoleBuffer.Write('-');
                            }
                        }
                    }

                    _consoleBuffer.WriteLine();

                    if (y == -1)
                    {
                        break;
                    }
                }
            }

            t.Stop();

            _consoleBuffer.WriteLine($"Took {t.ElapsedMilliseconds} milliseconds to render");
        }

        protected override void UpdateInternal()
        {
        }

        private readonly GameData _data;
        private readonly ConsoleBuffer _consoleBuffer;
    }
}
