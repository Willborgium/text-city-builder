﻿using System;
using System.Text;
using TextCityBuilder.Services;
using TextCityBuilder.States;

namespace TextCityBuilder
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var factory = new InitialGameStateFactory();
            var gameStateService = new GameStateService(factory);

            gameStateService.Run();
        }
    }
}
